import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reorderable_grid_view/reorderable_grid_view.dart';
import 'deck.dart';
import '../core.dart';

class SubjectPage extends StatefulWidget {
  const SubjectPage({super.key});

  @override
  State<SubjectPage> createState() => _SubjectPageState();
}

class _SubjectPageState extends State<SubjectPage> {
  final TextEditingController _controller = TextEditingController();
  List<Widget> lister(Subject model) {
    List<Widget> ret = [];
    ret.add(const DeckDisplay(isLast: true));
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    var subject = Provider.of<Subject>(context);
    var model = Provider.of<CoreModel>(context);
    _controller.text = subject.name;
    return Scaffold(
      appBar: AppBar(
          title: TextField(
              controller: _controller,
              onChanged: (value) {
                subject.rename(value);
              },
              decoration: const InputDecoration(border: InputBorder.none),
              style: const TextStyle(fontSize: 20.0)),
          actions: [
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: IconButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                                title: const Text('Delete subject'),
                                content: const Text(
                                    'Are you sure you want to delete this subject?'),
                                actions: [
                                  TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop(false);
                                      },
                                      child: const Text('No')),
                                  TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                      child: const Text('Yes',
                                          style: TextStyle(color: Colors.red))),
                                ])).then((value) {
                      if (value != null && value) {
                        Navigator.of(context).pop();
                        model.removeSubject(subject);
                      }
                    });
                  },
                  icon: const Icon(Icons.delete)),
            )
          ]),
      body: Center(
        child: Container(
            margin: const EdgeInsets.all(8.0),
            child: Consumer<Subject>(builder: (context, subject, child) {
              List<Widget> deckDisplays = [];
              for (var deck in subject.decks) {
                deckDisplays.add(
                  MultiProvider(
                      key: ValueKey(deck),
                      providers: [
                        ChangeNotifierProvider.value(value: deck),
                        ChangeNotifierProvider.value(
                            value: Provider.of<Subject>(context))
                      ],
                      child: const DeckDisplay(isLast: false)),
                );
              }
              deckDisplays.add(DeckDisplay(key: UniqueKey(), isLast: true));
              return ReorderableGridView.count(
                  crossAxisCount: 2,
                  dragStartDelay: Durations.medium1,
                  onReorder: (oldIndex, newIndex) async {
                    await subject.reorderInstant(oldIndex, newIndex);
                  },
                  dragEnableConfig: (index) {
                    return (index < deckDisplays.length - 1);
                  },
                  children: deckDisplays);
            })),
      ),
    );
  }
}

class SubjectDisplay extends StatelessWidget {
  const SubjectDisplay({super.key});

  @override
  Widget build(BuildContext context) {
    var subject = Provider.of<Subject>(context);
    var model = Provider.of<CoreModel>(context);
    return Card(
      elevation: 3,
      margin: const EdgeInsets.all(8.0),
      child: InkWell(
        borderRadius: BorderRadius.circular(8.0),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MultiProvider(
                        providers: [
                          ChangeNotifierProvider.value(value: subject),
                          ChangeNotifierProvider.value(value: model),
                        ],
                        child: const SubjectPage(),
                      )));
        },
        child: Center(
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                const Spacer(),
                Text(
                  subject.name,
                  style: const TextStyle(
                    fontSize: 18.0,
                  ),
                  textAlign: TextAlign.center,
                  softWrap: true,
                ),
                const Spacer(),
                const Divider(),
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    '${subject.deckCount()} decks',
                    style: const TextStyle(
                      fontSize: 18.0,
                      color: Colors.blueGrey,
                    ),
                    textAlign: TextAlign.right,
                    softWrap: true,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
