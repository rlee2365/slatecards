import 'dart:math';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../core.dart';
import 'cards.dart';
import 'extra.dart';

enum DeckView { list, cards }

class DeckDisplay extends StatelessWidget {
  final bool isLast;

  const DeckDisplay({required this.isLast, super.key});

  @override
  Widget build(BuildContext context) {
    if (!isLast) {
      var subject = Provider.of<Subject>(context);
      Deck deck = Provider.of<Deck>(context);
      return Card(
          elevation: 2,
          margin: const EdgeInsets.all(8.0),
          child: InkWell(
            borderRadius: BorderRadius.circular(8.0),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MultiProvider(providers: [
                            ChangeNotifierProvider.value(value: deck),
                            ChangeNotifierProvider.value(value: subject)
                          ], child: const DeckPage())));
            },
            child: Center(
              child: Container(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    const Spacer(),
                    Text(
                      deck.name,
                      style: const TextStyle(
                        fontSize: 18.0,
                      ),
                      textAlign: TextAlign.center,
                      softWrap: true,
                    ),
                    const Spacer(),
                    const Divider(),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        '${deck.length} cards',
                        style: const TextStyle(
                          fontSize: 18.0,
                          color: Colors.blueGrey,
                        ),
                        textAlign: TextAlign.right,
                        softWrap: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ));
    } else {
      return IconButton(
          icon: const Icon(Icons.add_circle_outline, size: 48.0),
          onPressed: () {
            var subject = Provider.of<Subject>(context, listen: false);
            showDialog(
                context: context,
                builder: (BuildContext context2) {
                  return NewDialog(
                      title: "New Deck",
                      hintText: "Enter name of new deck",
                      buttonText: 'OK',
                      onSubmitted: (String name) {
                        subject.addDeck(name);
                      });
                });
          });
    }
  }
}

class DeckPage extends StatefulWidget {
  const DeckPage({super.key});

  @override
  State<DeckPage> createState() => _DeckPageState();
}

class _DeckPageState extends State<DeckPage> {
  final TextEditingController _controller = TextEditingController();
  Set<DeckView> selected = {DeckView.list};
  Set<CardsOrder> orderSelected = {CardsOrder.ordered};
  Set<int> deletionIndexes = {};
  DeleteMode deleteMode = DeleteMode.none;
  CardsOrder cardsOrder = CardsOrder.ordered;
  List<(int, Flashcard)> listToUse = [];

  List<Widget> cardList(BuildContext context) {
    List<Widget> ret = [];
    for (final (index, card) in listToUse) {
      ret.add(ChangeNotifierProvider.value(
          value: card,
          key: ValueKey(card),
          child: CardListDisplay(
            index: index,
            deleteMode: deleteMode,
            deletionIndexes: deletionIndexes,
            cardsOrder: cardsOrder,
          )));
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    Deck deck = Provider.of<Deck>(context);
    var model = Provider.of<Subject>(context, listen: false);
    _controller.text = deck.name;
    listToUse = List.from(deck.flashcards.indexed);
    if (cardsOrder == CardsOrder.random) {
      listToUse = List.from(listToUse)..shuffle(Random());
    }
    return Scaffold(
      appBar: AppBar(
        title: TextField(
            controller: _controller,
            onChanged: (value) {
              deck.rename(value);
            },
            decoration: const InputDecoration(border: InputBorder.none),
            style: const TextStyle(fontSize: 20.0)),
        actions: [
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: IconButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                              title: const Text('Delete deck'),
                              content: const Text(
                                  'Are you sure you want to delete this deck?'),
                              actions: [
                                TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop(false);
                                    },
                                    child: const Text('No')),
                                TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop(true);
                                    },
                                    child: const Text('Yes',
                                        style: TextStyle(color: Colors.red))),
                              ])).then((value) {
                    if (value != null && value) {
                      Navigator.of(context).pop();
                      model.removeDeck(deck);
                    }
                  });
                },
                icon: const Icon(Icons.delete)),
          )
        ],
      ),
      body: Consumer<Deck>(builder: (context, deck, child) {
        if (selected.contains(DeckView.list)) {
          assert(!selected.contains(DeckView.cards));
          return ReorderableListView(
            buildDefaultDragHandles: false,
            onReorder: (oldIndex, newIndex) {
              deck.reorderInstant(oldIndex, newIndex);
            },
            children: cardList(context),
          );
        } else {
          return CardDisplay(
            cardsOrder: cardsOrder,
            listToUse: listToUse,
          );
        }
      }),
      bottomNavigationBar: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                // remove button
                padding: const EdgeInsets.all(8.0),
                child: ActionChip(
                  label: cardsOrder == CardsOrder.ordered
                      ? const Icon(Icons.shuffle)
                      : const Icon(Icons.shuffle_on_outlined),
                  onPressed: () {
                    setState(() {
                      if (cardsOrder == CardsOrder.ordered) {
                        cardsOrder = CardsOrder.random;
                      } else {
                        cardsOrder = CardsOrder.ordered;
                      }
                    });
                  },
                ),
              ),
              Padding(
                // remove button
                padding: const EdgeInsets.all(8.0),
                child: ActionChip(
                  label: () {
                    if (cardsOrder == CardsOrder.random ||
                        selected.contains(DeckView.cards)) {
                      return const Icon(Icons.remove_circle_outline,
                          color: Colors.grey);
                    } else {
                      return deleteMode == DeleteMode.none
                          ? const Icon(Icons.remove_circle_outline,
                              color: Colors.red)
                          : const Icon(Icons.remove_circle, color: Colors.red);
                    }
                  }(),
                  onPressed: () {
                    if (cardsOrder == CardsOrder.random ||
                        selected.contains(DeckView.cards)) {
                      return; // noop
                    }
                    if (deleteMode == DeleteMode.none) {
                      setState(() {
                        deleteMode = DeleteMode.deleteReady;
                      });
                    } else {
                      if (deletionIndexes.isEmpty) {
                        setState(() {
                          deleteMode = DeleteMode.none;
                        });
                        return;
                      }
                      showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                                  title:
                                      const Text('Delete selected flashcards'),
                                  content: const Text(
                                      'Are you sure you want to delete the selected flashcards?'),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop(false);
                                        },
                                        child: const Text('Cancel')),
                                    TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop(true);
                                        },
                                        child: const Text('Yes',
                                            style:
                                                TextStyle(color: Colors.red))),
                                  ])).then((value) async {
                        if (value) {
                          deleteMode = DeleteMode.none;
                          List<int> deletionIndexesList =
                              deletionIndexes.toList();
                          deletionIndexesList.sort((a, b) => b.compareTo(a));
                          for (var index in deletionIndexesList) {
                            await deck.removeCardAt(index);
                          }
                          setState(() {});
                        }
                      });
                    }
                  },
                ),
              ),
              Padding(
                // add button
                padding: const EdgeInsets.all(8.0),
                child: ActionChip(
                  label: Icon(Icons.add,
                      color: selected.contains(DeckView.cards)
                          ? Colors.grey
                          : null),
                  onPressed: () {
                    if (selected.contains(DeckView.cards)) {
                      return;
                    }
                    deck.addCard();
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: SegmentedButton(
                  segments: const [
                    ButtonSegment(value: DeckView.list, icon: Icon(Icons.list)),
                    ButtonSegment(
                        value: DeckView.cards,
                        icon: Icon(Icons.content_copy_sharp))
                  ],
                  selected: selected,
                  onSelectionChanged: (newSelection) {
                    setState(() {
                      selected = newSelection;
                    });
                  },
                  showSelectedIcon: false,
                ),
              ),
            ],
          )),
    );
  }
}
