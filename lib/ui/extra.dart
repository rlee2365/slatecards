import 'package:flutter/material.dart';

enum CardsOrder { ordered, random }

class NewDialog extends StatelessWidget {
  final String title;
  final String hintText;
  final String buttonText;
  final void Function(String) onSubmitted;

  NewDialog(
      {required this.title,
      required this.hintText,
      required this.buttonText,
      required this.onSubmitted,
      super.key});

  final TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: TextField(
        controller: _textController,
        decoration: InputDecoration(hintText: hintText),
      ),
      actions: <Widget>[
        TextButton(
          child: const Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text(buttonText),
          onPressed: () {
            String enteredText = _textController.text;
            onSubmitted(enteredText);
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
