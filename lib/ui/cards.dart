import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../core.dart';
import 'extra.dart';
import 'dart:math';

enum DeleteMode { none, deleteReady }

enum CardFace { front, back }

class CardListDisplay extends StatefulWidget {
  final int index;
  final DeleteMode deleteMode;
  final Set<int> deletionIndexes;
  final CardsOrder cardsOrder;

  const CardListDisplay(
      {required this.index,
      this.deleteMode = DeleteMode.none,
      required this.deletionIndexes,
      required this.cardsOrder,
      super.key});

  @override
  State<CardListDisplay> createState() => _CardListDisplayState();
}

class _CardListDisplayState extends State<CardListDisplay> {
  bool readyToDelete = false;
  final _frontController = TextEditingController(),
      _backController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var card = Provider.of<Flashcard>(context);
    _frontController.text = card.front;
    _backController.text = card.back;
    if (widget.deleteMode == DeleteMode.none) {
      readyToDelete = false;
    }
    return Card(
      color: widget.deleteMode == DeleteMode.deleteReady
          ? const Color.fromARGB(255, 128, 23, 23)
          : null,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12.0, 0.0, 8.0, 0.0),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: TextField(
                    controller: _frontController,
                    textAlign: TextAlign.left,
                    decoration: const InputDecoration(border: InputBorder.none),
                    maxLines: null,
                    onChanged: (value) {
                      card.setFront(value);
                    }),
              ),
              const VerticalDivider(width: 20.0),
              Expanded(
                child: TextField(
                    controller: _backController,
                    textAlign: TextAlign.left,
                    decoration: const InputDecoration(border: InputBorder.none),
                    maxLines: null,
                    onChanged: (value) {
                      card.setBack(value);
                    }),
              ),
              const VerticalDivider(width: 5.0),
              SizedBox(
                width: widget.cardsOrder != CardsOrder.random ? 50 : 0,
                child: Builder(builder: (context) {
                  if (widget.cardsOrder == CardsOrder.random) {
                    return Container();
                  }
                  if (widget.deleteMode == DeleteMode.none) {
                    return ReorderableDragStartListener(
                        index: widget.index,
                        child: const Center(child: Icon(Icons.drag_indicator)));
                  } else {
                    return Center(
                        child: Checkbox(
                            value: readyToDelete,
                            visualDensity: VisualDensity.compact,
                            onChanged: (v) {
                              setState(() {
                                bool v_ = v!;
                                readyToDelete = v_;
                                if (v_) {
                                  widget.deletionIndexes.add(widget.index);
                                } else {
                                  widget.deletionIndexes.remove(widget.index);
                                }
                              });
                            }));
                  }
                }),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class CardDisplay extends StatefulWidget {
  final CardsOrder cardsOrder;
  final List<(int, Flashcard)> listToUse;
  const CardDisplay(
      {required this.cardsOrder, required this.listToUse, super.key});

  @override
  State<CardDisplay> createState() => _CardDisplayState();
}

class _CardDisplayState extends State<CardDisplay>
    with TickerProviderStateMixin {
  int orderedIndex = 0, randomIndex = 0;

  late AnimationController _flipController;
  late Animation _flipAnimation;
  AnimationStatus _status = AnimationStatus.dismissed;
  late AnimationController _cardTransitionController;
  CardFace cardFace = CardFace.front;

  late Animation _cardTransitionAnimation;

  @override
  void initState() {
    super.initState();
    _flipController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 100));
    _flipAnimation = Tween(end: 1.0, begin: 0.0).animate(_flipController)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        _status = status;
      });
    _cardTransitionController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 60));
    _cardTransitionAnimation = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 8.0, end: 0.0), weight: 1),
    ]).animate(_cardTransitionController);
  }

  @override
  Widget build(BuildContext context) {
    Deck deck = Provider.of<Deck>(context);
    orderedIndex = orderedIndex.clamp(0, deck.flashcards.length - 1);
    randomIndex = randomIndex.clamp(0, deck.flashcards.length - 1);
    var useIndex =
        widget.cardsOrder == CardsOrder.ordered ? orderedIndex : randomIndex;
    Flashcard flashcard = widget.listToUse[useIndex].$2;
    if (deck.flashcards.isEmpty) {
      return const Padding(
        padding: EdgeInsets.all(8.0),
        child: Card(
            child: Column(
          children: [
            Spacer(),
            Text("There are no flashcards in this deck."),
            Spacer(),
            SizedBox(width: double.infinity)
          ],
        )),
      );
    }
    return AnimatedBuilder(
        animation: _cardTransitionController,
        builder: (_, __) {
          return Padding(
            padding: EdgeInsets.all(_cardTransitionAnimation.value),
            child: GestureDetector(
              onHorizontalDragUpdate: (details) {
                double distance = details.primaryDelta ?? 0;
                if (_flipController.isAnimating) {
                  return;
                }
                if (distance > 0) {
                  // swiped right
                  _flipController.reverse();
                  cardFace = CardFace.front;
                } else if (distance < -0) {
                  // swiped left
                  if (cardFace == CardFace.front &&
                      _status == AnimationStatus.dismissed) {
                    _flipController.forward();
                    cardFace = CardFace.back;
                  }
                }
              },
              onVerticalDragUpdate: (details) {
                double distance = details.primaryDelta ?? 0;
                if (_cardTransitionController.isAnimating) {
                  return;
                }
                if (distance > 0) {
                  // swiped down
                  if (useIndex > 0) {
                    _cardTransitionController.forward().then((v) {
                      setState(() {
                        if (widget.cardsOrder == CardsOrder.ordered) {
                          orderedIndex -= 1;
                        } else if (widget.cardsOrder == CardsOrder.random) {
                          randomIndex -= 1;
                        }
                        cardFace = CardFace.front;
                        _flipController.reset();
                      });
                      _cardTransitionController.reverse();
                    });
                  }
                } else if (distance < -0) {
                  // swiped up
                  if (useIndex < deck.length - 1) {
                    _cardTransitionController.forward().then((v) {
                      setState(() {
                        if (widget.cardsOrder == CardsOrder.ordered) {
                          orderedIndex += 1;
                        } else if (widget.cardsOrder == CardsOrder.random) {
                          randomIndex += 1;
                        }
                        cardFace = CardFace.front;
                        _flipController.reset();
                      });
                      _cardTransitionController.reverse();
                    });
                  }
                }
              },
              child: Transform(
                alignment: FractionalOffset.center,
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.0015)
                  ..rotateY(-pi * _flipAnimation.value),
                child: Transform.scale(
                  scaleX: _flipAnimation.value <= 0.5 ? 1 : -1,
                  child: Card(
                      child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              width: double.infinity,
                              child: Text(
                                (useIndex + 1).toString(),
                                textAlign: TextAlign.right,
                              )),
                          Text(
                              _flipAnimation.value <= 0.5
                                  ? flashcard.front
                                  : flashcard.back,
                              style: const TextStyle(fontSize: 20)),
                          const Spacer(),
                          const SizedBox(width: double.infinity)
                        ]),
                  )),
                ),
              ),
            ),
          );
        });
  }
}
