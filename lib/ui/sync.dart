import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:platform_info/platform_info.dart';
import 'package:sodium/sodium.dart';
import 'package:crdt/crdt.dart';
import '../sync2.dart';

class SyncPage extends StatefulWidget {
  const SyncPage({super.key});

  @override
  State<SyncPage> createState() => _SyncPageState();
}

class _SyncPageState extends State<SyncPage> {
  @override
  Widget build(BuildContext context) {
    var syncService = Provider.of<SyncService2>(context);
    syncService.keyChallengeCallback =
        (sodium, ckey, skey) => keyChallenge(context, sodium, ckey, skey);
    List<Widget> serviceWidgets = [];

    for (var service in syncService.getServices()) {
      String subtitle = '';
      if (syncService.isBroadcasting() &&
          syncService.serviceName == service.name) {
        subtitle = 'Current device';
      }

      serviceWidgets.add(Card(
          child: InkWell(
              onTap: (var serviceName) {
                // Does this lambda capture even work?
                // Unfortunately we cannot tell without a second device...
                return () async {
                  if (syncService.serviceName == service.name) {
                    return;
                  }
                  await syncService.resolveService(serviceName);
                };
              }(service.name),
              borderRadius: BorderRadius.circular(12.0),
              child: ListTile(
                  title: Text(service.name), subtitle: Text(subtitle)))));
    }

    return Scaffold(
        appBar: AppBar(title: const Text('Sync')),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                  width: double.maxFinite, child: Text('Instances found:')),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 4.0),
                child: SingleChildScrollView(
                    child: Column(children: serviceWidgets)),
              )),
              const Divider(),
              SizedBox(
                width: double.maxFinite,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Broadcast on LAN'),
                    Switch(
                      value: syncService.isBroadcasting(),
                      onChanged: (v) async {
                        if (v == true) {
                          await syncService.setupBroadcast();
                          setState(() {});
                        } else {
                          await syncService.stopBroadcast();
                          setState(() {});
                        }
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 8),
              SizedBox(
                height: 45,
                width: double.maxFinite,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(syncService.isBroadcasting()
                          ? 'Broadcasting as ${syncService.serviceName}'
                          : ' '),
                    ),
                    const SizedBox(width: 20),
                    //IconButton.outlined(
                    //  icon: Icon(Icons.qr_code),
                    //  onPressed: () async {
                    //    var data = await RSA.encryptOAEP(
                    //        syncService.keyPair!.publicKey +
                    //            syncService.currentPassphrase!,
                    //        '',
                    //        Hash.SHA256,
                    //        syncService.keyPair!.publicKey);
                    //    debugPrint("Orig: ${utf8.encode(data).length}");
                    //    debugPrint(
                    //        "Gzip: ${gzip.encode(utf8.encode(data)).length}");
                    //    qrDialog(context, '', data);
                    //  },
                    //)
                  ],
                ),
              ),
              const SizedBox(height: 4),
              const Divider(),
              SizedBox(
                  height: 50,
                  width: double.maxFinite,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 50,
                        child: SingleChildScrollView(
                          child: Text('Sync status: ${syncService.syncStatus}',
                              style:
                                  const TextStyle(overflow: TextOverflow.fade)),
                        ),
                      ),
                      //IconButton(
                      //    icon: const Icon(Icons.question_mark),
                      //    onPressed: () async {
                      //      debugPrint(
                      //          '${(await syncService.coreModel.getChangeSet()).length}');
                      //    })
                    ],
                  ))
            ],
          ),
        ));
  }
}

Future<bool> keyChallenge(BuildContext context, Sodium sodium, String clientKey,
    String serverKey) async {
  var signature = base64Encode(sodium.crypto.genericHash(
      message: (clientKey + serverKey).toCharArray().unsignedView(),
      outLen: sodium.crypto.genericHash.bytes));
  var result = await showDialog<bool>(
      context: context,
      builder: (c) => AlertDialog(
              title: const Text('Key challenge'),
              content: Text(
                  'Confirm that the following signature matches on other device: $signature'),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context, true);
                    },
                    child: const Text('Yes')),
                TextButton(
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                    child: const Text('No')),
              ]));
  return (result != null) ? result : false;
}
