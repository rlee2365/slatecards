import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData slatecardsTheme() {
  var theme = ThemeData(
    fontFamily: GoogleFonts.ubuntu().fontFamily,
    colorScheme: ColorScheme.fromSeed(
      brightness: Brightness.dark,
      seedColor: const Color(0xff131b23),
    ),
    appBarTheme: const AppBarTheme(backgroundColor: Color(0xff202020)),
    bottomAppBarTheme: const BottomAppBarTheme(
        color: Color(0xff202020),
        surfaceTintColor: Color(0xffffffff),
        elevation: 0),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: Color(0xff292c2f), elevation: 1),
    // c9f0ff eafffd e9d758
    useMaterial3: true,
  );
  return theme;
}
