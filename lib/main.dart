import 'package:flutter/material.dart';
import 'core.dart';
import 'ui/extra.dart';
import 'package:provider/provider.dart';
import 'package:reorderable_grid_view/reorderable_grid_view.dart';
import 'package:window_manager/window_manager.dart';
import 'package:platform_info/platform_info.dart';
import 'ui/styling.dart';
import 'ui/subjects.dart';
import 'ui/sync.dart';
import 'package:sodium_libs/sodium_libs.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'sync2.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final sodium = await SodiumInit.init();
  await CoreModelSingle.init();

  sqfliteFfiInit();
  databaseFactory = databaseFactoryFfi;

  SyncService2 syncService =
      SyncService2(coreModel: CoreModelSingle.coreModel, sodium: sodium);
  await syncService.loadPersistentState();
  await syncService.setupDiscovery();

  if (platform.isWindows) {
    await windowManager.ensureInitialized();
    WindowOptions windowOptions = const WindowOptions(size: Size(450, 800));
    await windowManager.waitUntilReadyToShow(windowOptions, () async {
      await windowManager.setAspectRatio(9 / 16);
      await windowManager.setMinimumSize(const Size(450, 800));
      await windowManager.show();
      await windowManager.focus();
    });
  }
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider.value(value: syncService),
    ChangeNotifierProvider.value(value: CoreModelSingle.coreModel)
  ], child: const SlatecardsApp()));

  // Flutter does not actually provide a way for us to determine when the app is closed.
  // So cleanup work is expected to be handled by libraries?
}

class SlatecardsApp extends StatelessWidget {
  const SlatecardsApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'slatecards',
      theme: slatecardsTheme(),
      home: const Frontpage(title: 'slatecards'),
    );
  }
}

class Frontpage extends StatefulWidget {
  final String title;
  const Frontpage({required this.title, super.key});

  @override
  State<Frontpage> createState() => _FrontpageState();
}

class _FrontpageState extends State<Frontpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Center(child: Text(widget.title)), actions: [
          IconButton(
              onPressed: () async {
                await Provider.of<SyncService2>(context, listen: false)
                    .refreshHook();
              },
              icon: const Icon(Icons.refresh)),
          IconButton(
            icon: const Icon(Icons.wifi),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const SyncPage()));
            },
          ),
        ]),
        body: Builder(builder: (context) {
          Provider.of<SyncService2>(context, listen: false).snackbarCallback =
              (s) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(s)));
          };
          return Container(
              margin: const EdgeInsets.all(8.0),
              child: Consumer<CoreModel>(builder: (context, model, child) {
                List<Widget> subjectDisplays = [];
                for (var subject in model.subjects) {
                  subjectDisplays.add(MultiProvider(providers: [
                    ChangeNotifierProvider.value(value: subject),
                    ChangeNotifierProvider.value(value: model)
                  ], key: ValueKey(subject), child: const SubjectDisplay()));
                }
                subjectDisplays.add(IconButton(
                  key: UniqueKey(),
                  icon: const Icon(Icons.add_circle_outline, size: 48.0),
                  onPressed: () {
                    var model = Provider.of<CoreModel>(context, listen: false);
                    showDialog(
                        context: context,
                        builder: (context2) {
                          return NewDialog(
                              title: "New Subject",
                              hintText: "Enter name of new subject",
                              buttonText: 'OK',
                              onSubmitted: (String name) {
                                model.addSubject(name);
                              });
                        });
                  },
                ));
                return ReorderableGridView.count(
                    onReorder: (oldIndex, newIndex) async {
                      await model.reorderInstant(oldIndex, newIndex);
                    },
                    dragStartDelay: Durations.medium1,
                    crossAxisCount: 2,
                    children: subjectDisplays,
                    dragEnableConfig: (index) {
                      return (index < subjectDisplays.length - 1);
                    });
              }));
        }));
  }
}
