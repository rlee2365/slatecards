import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:bonsoir/bonsoir.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:path_provider/path_provider.dart';
import 'package:platform_info/platform_info.dart';
import 'package:slatecards/core.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'package:word_generator/word_generator.dart';
import 'package:shelf_router/shelf_router.dart' as sr;
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as shelf_io;
import 'package:http/http.dart' as http;
import 'package:sqlite_crdt/sqlite_crdt.dart';
import 'package:crdt/crdt.dart';
import 'package:sodium/sodium.dart';

import 'dart:io';

typedef UpdateSyncStatusCallback = void Function(String);
typedef UpdateSyncHlcCallback = void Function(Hlc);
typedef IsPeerAuthCallback = Future<bool> Function(String);
typedef AddPeerCallback = Future<void> Function(String, String?);
typedef KeyChallengeCallback = Future<bool> Function(Sodium, String, String);
typedef HlcOfLastSyncCallback = Future<Hlc?> Function(String);
typedef SnackbarCallback = void Function(String);

class _Session {
  String clientPublicKey;

  _Session({this.clientPublicKey = ''});
}

class KeyPairUtil {
  late String publicKey, privateKey;

  KeyPairUtil(KeyPair kp) {
    privateKey = base64Encode(kp.secretKey.extractBytes());
    publicKey = base64Encode(kp.publicKey);
  }
  KeyPairUtil.fromBase64(this.publicKey, this.privateKey);

  String makeNonce(Sodium sodium) {
    return base64Encode(
        sodium.randombytes.buf(sodium.crypto.secretBox.nonceBytes));
  }

  String encrypt(
      Sodium sodium, String input, String nonce, String receiverKey) {
    return base64Encode(sodium.crypto.box.easy(
        message: input.toCharArray().unsignedView(),
        nonce: base64Decode(nonce),
        publicKey: base64Decode(receiverKey),
        secretKey: SecureKey.fromList(sodium, base64Decode(privateKey))));
  }

  String decrypt(Sodium sodium, String input, String nonce, String senderKey) {
    return String.fromCharCodes(sodium.crypto.box.openEasy(
        cipherText: base64Decode(input),
        nonce: base64Decode(nonce),
        publicKey: base64Decode(senderKey),
        secretKey: SecureKey.fromList(sodium, base64Decode(privateKey))));
  }
}

class SyncClientHandle {
  String? host;
  http.Client? client;
  String? sessionToken;
  String? serverPublicKey;
  KeyPairUtil keyPair;
  UpdateSyncStatusCallback updateSyncStatusCallback;
  //HlcOfLastSyncCallback hlcOfLastSyncCallback;
  late UpdateSyncHlcCallback updateSyncHlcCallback;
  late KeyChallengeCallback keyChallengeCallback;
  IsPeerAuthCallback isPeerAuthCallback;
  AddPeerCallback addPeerCallback;
  late CoreModel coreModel;
  static const int maxPinAttempts = 3;

  SyncClientHandle(
      {required this.keyPair,
      required this.updateSyncStatusCallback,
      //required this.hlcOfLastSyncCallback,
      required this.coreModel,
      required this.isPeerAuthCallback,
      required this.addPeerCallback});

  void connect(Sodium sodium, String arghost, String servicename) async {
    host = arghost;
    client = http.Client();
    debugPrint('connect called for host $arghost');

    var publicKeyResponse = await client!.get(Uri.http(host!, '/publickey'));
    if (publicKeyResponse.statusCode != 200) {
      updateSyncStatusCallback(
          'Failed to get server public key with status ${publicKeyResponse.statusCode}');
      return;
    }
    serverPublicKey = publicKeyResponse.body;

    var verifyResponse = await client!.post(Uri.http(host!, '/verify'),
        body: jsonEncode({'clientPublicKey': keyPair.publicKey}));
    if (verifyResponse.statusCode != 200) {
      updateSyncStatusCallback(
          'Failed to get server verification with status ${verifyResponse.statusCode}');
      return;
    }
    bool isVerified = jsonDecode(verifyResponse.body)['verified'];

    bool isKnownToClient = await isPeerAuthCallback(serverPublicKey!);
    if (!isKnownToClient || !isVerified) {
      var exchangeFuture = client!.post(Uri.http(host!, '/exchange'),
          body: jsonEncode({
            'clientPublicKey': keyPair.publicKey,
            'isKnownToClient': isKnownToClient
          }));

      // Do not rely on the server to tell us whether the client is known or not.
      if (await keyChallengeCallback(
          sodium, keyPair.publicKey, serverPublicKey!)) {
        await addPeerCallback(serverPublicKey!, servicename);
        updateSyncStatusCallback('Key exchange accepted on client side.');
      } // Otherwise we do nothing

      var exchangeResponse = await exchangeFuture;
      if (exchangeResponse.statusCode != 200) {
        updateSyncStatusCallback(
            'Failed to exchange keys with status ${exchangeResponse.statusCode}');
        return;
      }
      updateSyncStatusCallback('Key exchange successful; synchronizing...');
      var nonce = exchangeResponse.headers['slatecards-nonce']!;
      var body = jsonDecode(keyPair.decrypt(
          sodium, exchangeResponse.body, nonce, serverPublicKey!));
      sessionToken = body['sessionToken'];
    } else {
      var exchangeResponse = await client!.post(Uri.http(host!, '/exchange'),
          body: jsonEncode({'clientPublicKey': keyPair.publicKey}));
      if (exchangeResponse.statusCode != 200) {
        updateSyncStatusCallback(
            'Failed to get session with status ${exchangeResponse.statusCode}');
        return;
      }
      var nonce = exchangeResponse.headers['slatecards-nonce']!;
      var body = jsonDecode(keyPair.decrypt(
          sodium, exchangeResponse.body, nonce, serverPublicKey!));
      sessionToken = body['sessionToken'];
    }
    await sync(sodium);
  }

  Future<Hlc?> getServerHlc() async {
    var hlcResponse = await client!.get(Uri.http(host!, '/hlc'));
    if (hlcResponse.statusCode != 200) {
      updateSyncStatusCallback(
          'Failed to get server sync hlc with status ${hlcResponse.statusCode}');
      return null;
    }
    return hlcResponse.body!.toHlc;
  }

  Future<void> sync(Sodium sodium) async {
    String changeset = await coreModel.getChangeSet();
    var body = jsonEncode({
      'sessionToken': sessionToken,
      'changeset': changeset,
    });
    var syncNonce = keyPair.makeNonce(sodium);

    var syncResponse = await client!.post(Uri.http(host!, '/sync'),
        body: keyPair.encrypt(sodium, body, syncNonce, serverPublicKey!),
        headers: {
          'slatecards-nonce': syncNonce,
          'slatecards-clientkey': keyPair.publicKey
        });
    if (syncResponse.statusCode != 200) {
      updateSyncStatusCallback(
          'Failed to sync with status ${syncResponse.statusCode}, body: ${syncResponse.body}');
      return;
    }
    syncNonce = syncResponse.headers['slatecards-nonce']!;
    var syncBody = jsonDecode(keyPair.decrypt(
        sodium, syncResponse.body, syncNonce, serverPublicKey!));

    if (syncResponse.statusCode == 200 && syncBody['status'] == 'sync_ok') {
      updateSyncStatusCallback('Sync accepted, merging server changes');
      CrdtChangeset decryptedChangeSet =
          parseCrdtChangeset(jsonDecode((syncBody['changeset'])));
      await coreModel.mergeChangeSet(decryptedChangeSet);
      updateSyncStatusCallback('Sync successful');
    }
  }
}

class SyncService2 extends ChangeNotifier {
  late Database db;
  final storage = const FlutterSecureStorage();
  bool _syncSetting = false;

  bool get syncSetting => _syncSetting;
  Future<void> setSyncSetting(bool value) async {
    _syncSetting = value;
    await storage.write(key: 'syncSetting', value: value.toString());
  }

  BonsoirService? service;
  BonsoirDiscovery? discovery;
  BonsoirBroadcast? broadcast;
  static const String serviceType = '_slatecards-sync._tcp';
  static const String servicePrefix = 'slatecards sync service-';
  static const int servicePort = 32298;
  final List<BonsoirService> _services = [];

  HttpServer? serverHandle;

  late SyncClientHandle clientHandle;
  late KeyChallengeCallback _keyChallengeCallback;
  late SnackbarCallback snackbarCallback;

  set keyChallengeCallback(KeyChallengeCallback value) {
    _keyChallengeCallback = value;
    clientHandle.keyChallengeCallback = _keyChallengeCallback;
  }

  late KeyPairUtil keyPair;
  CoreModel coreModel;
  Sodium sodium;
  late String currentPassphrase;
  late String serviceName;

  String? desiredServiceName;
  String syncStatus = '';

  Map<String, _Session> _sessions = {};

  SyncService2({required this.coreModel, required this.sodium});

  Future<void> loadPersistentState() async {
    String? hostname;
    if (!await storage.containsKey(key: 'hostnamev2')) {
      final wordGenerator = WordGenerator();
      hostname = '$servicePrefix${wordGenerator.randomNouns(3).join('-')}';
      await storage.write(key: 'hostnamev2', value: hostname);
    } else {
      hostname = await storage.read(key: 'hostnamev2');
    }

    if (!await storage.containsKey(key: 'privateKeyv2')) {
      var newKeyPair = KeyPairUtil(sodium.crypto.box.keyPair());
      await storage.write(key: 'privateKeyv2', value: newKeyPair.privateKey);
      await storage.write(key: 'publicKeyv2', value: newKeyPair.publicKey);
      keyPair = newKeyPair;
    } else {
      keyPair = KeyPairUtil.fromBase64(
          (await storage.read(key: 'publicKeyv2'))!,
          (await storage.read(key: 'privateKeyv2'))!);
    }

    if (!await storage.containsKey(key: 'syncSetting')) {
      await storage.write(key: 'syncSetting', value: false.toString());
    } else {
      await setSyncSetting(await storage.read(key: 'syncSetting') == 'true');
    }

    clientHandle = SyncClientHandle(
        keyPair: keyPair,
        updateSyncStatusCallback: updateSyncStatus,
        coreModel: coreModel,
        isPeerAuthCallback: isPeerAuth,
        addPeerCallback: addPeer);

    late String dbParent;
    if (platform.isAndroid || platform.isIOS || platform.isWindows) {
      dbParent = (await getApplicationDocumentsDirectory()).path;
    } else {
      dbParent = (await getLibraryDirectory()).path;
    }

    // Peers are addressed by public key
    //db = await openDatabase(inMemoryDatabasePath);
    var dbFile = File('$dbParent/slatecards_peers.db');
    //if (dbFile.existsSync()) {
    //  dbFile.deleteSync();
    //} //
    db = await openDatabase(dbFile.path);
    await db.execute('''
      CREATE TABLE IF NOT EXISTS peers (
        publicKey TEXT PRIMARY KEY,
        serviceName TEXT
      );
    ''');

    final wordGenerator = WordGenerator();
    currentPassphrase = wordGenerator.randomNouns(5).join(' ');

    serviceName = hostname!;
  }

  Future<bool> isPeerAuth(String publicKey) async {
    return (await db
            .query('peers', where: 'publicKey = ?', whereArgs: [publicKey]))
        .isNotEmpty;
  }

  Future<bool> isPeerAuthByName(String name) async {
    return (await db
            .query('peers', where: 'serviceName = ?', whereArgs: [name]))
        .isNotEmpty;
  }

  //Future<Hlc?> hlcOfLastSync(String publicKey) async {
  //  var records =
  //      await db.query('peers', where: 'publicKey = ?', whereArgs: [publicKey]);
  //  if (records.isEmpty) {
  //    return null;
  //  }
  //  for (var record in records) {
  //    var hlcRecord = record['hlcOfLastSync'];
  //    if (hlcRecord == null) {
  //      return null;
  //    }
  //    return (record['hlcOfLastSync']! as String).toHlc;
  //  }
  //}

  //Future<void> updateHlcOfLastSync(String publicKey, Hlc hlc) async {
  //  await db.update('peers', {'hlcOfLastSync': hlc.toString()},
  //      where: 'publicKey = ?', whereArgs: [publicKey]);
  //}

  Future<void> addPeer(String publicKey, String? serviceName) async {
    (await db
        .insert('peers', {'publicKey': publicKey, 'serviceName': serviceName}));
  }

  void updateSyncStatus(String s) {
    debugPrint(s);
    syncStatus = s;
    notifyListeners();
  }

  Future<void> setupDiscovery() async {
    discovery = BonsoirDiscovery(type: serviceType);
    await discovery!.ready;
    discovery!.eventStream!.listen((event) {
      if (event.type == BonsoirDiscoveryEventType.discoveryServiceFound) {
        if (event.service!.name.startsWith(servicePrefix)) {
          _services.add(event.service!);
          notifyListeners();
        }
      } else if (event.type ==
          BonsoirDiscoveryEventType.discoveryServiceResolved) {
        _services.removeWhere((s) => event.service!.name == s.name);
        _services.add(event.service!);
        if (desiredServiceName == event.service!.name) {
          assert(event.service! is ResolvedBonsoirService);
          var resolvedService = event.service! as ResolvedBonsoirService;
          clientHandle.connect(
              sodium,
              '${resolvedService.host!}:${resolvedService.port}',
              resolvedService.name);
          updateSyncStatus('Connecting to ${event.service!.name}');
        }
        notifyListeners();
      } else if (event.type == BonsoirDiscoveryEventType.discoveryServiceLost) {
        // Theoretically, a Ttl <= 0 should cause a "Lost" event to be transmitted
        // Unfortunately because the Ttl component is implemented in C++
        // There is no way for us to actually see it.
        _services.removeWhere((s) => event.service!.name == s.name);
        notifyListeners();
      }
    });
    await discovery!.start();
  }

  List<BonsoirService> getServices() => _services;
  Future<void> resolveService(String name) async {
    var selectedService = _services.firstWhere((s) => s.name == name);
    desiredServiceName = name;
    await selectedService.resolve(discovery!.serviceResolver);
  }

  Future<void> refreshHook() async {
    int numSyncable = 0;
    for (service in _services) {
      if (service is! ResolvedBonsoirService) {
        if (!await isPeerAuthByName(service!.name)) {
          continue;
        }
        numSyncable++;
        await resolveService(service!.name);
      } else {
        numSyncable++;
        var resolvedService = service as ResolvedBonsoirService;
        clientHandle.connect(
            sodium,
            '${resolvedService.host!}:${resolvedService.port}',
            resolvedService.name);
        updateSyncStatus('Connecting to ${resolvedService.name}');
      }
    }
    if (numSyncable == 0) {
      snackbarCallback('No syncable servers found.');
    } else {
      snackbarCallback('Sync on $numSyncable services.');
    }
  }

  Future<String> setupBroadcast() async {
    service =
        BonsoirService(name: serviceName, type: serviceType, port: servicePort);
    broadcast = BonsoirBroadcast(service: service!);

    await broadcast!.ready;
    await broadcast!.start();

    await setupServer();

    notifyListeners();

    return serviceName;
  }

  Future<void> setupServer() async {
    var _router = sr.Router();
    _router.get('/publickey', (Request request) async {
      return Response.ok(keyPair.publicKey);
    });
    _router.get('/hlc', (Request request) async {
      return Response.ok((await coreModel.getLatestHlc()).toString());
    });
    _router.post('/verify', (Request request) async {
      String clientPublicKey =
          jsonDecode(await request.readAsString())['clientPublicKey'];
      return Response.ok(
          jsonEncode({'verified': await isPeerAuth(clientPublicKey)}));
    });
    _router.post('/exchange', (Request request) async {
      // {'clientPublicKey': clientPublicKey}
      var body = jsonDecode(await request.readAsString());
      String clientPublicKey = body['clientPublicKey'];
      var sessionToken = generateSessionToken();

      if (!await isPeerAuth(clientPublicKey) || body['isKnownToClient']) {
        if (await _keyChallengeCallback(
            sodium, clientPublicKey, keyPair.publicKey)) {
          await addPeer(clientPublicKey, null);
          _sessions[sessionToken] = _Session(clientPublicKey: clientPublicKey);
          String body = jsonEncode({'sessionToken': sessionToken});
          var nonce = keyPair.makeNonce(sodium);
          updateSyncStatus(
              'Accepted authentication from client $clientPublicKey, added to known peers');
          return Response.ok(
              keyPair.encrypt(sodium, body, nonce, clientPublicKey),
              headers: {'slatecards-nonce': nonce});
        } else {
          updateSyncStatus(
              'Failed authentication from client $clientPublicKey');
          return Response.unauthorized('');
        }
      } else {
        _sessions[sessionToken] = _Session(clientPublicKey: clientPublicKey);
        String body = jsonEncode({'sessionToken': sessionToken});
        var nonce = keyPair.makeNonce(sodium);
        updateSyncStatus('Known authentication from client $clientPublicKey');
        return Response.ok(
            keyPair.encrypt(sodium, body, nonce, clientPublicKey),
            headers: {'slatecards-nonce': nonce});
      }
    });
    _router.post('/sync', (Request request) async {
      var syncNonce = request.headers['slatecards-nonce']!;
      var clientPublicKey = request.headers['slatecards-clientkey']!;
      var body = jsonDecode(await keyPair.decrypt(
          sodium, await request.readAsString(), syncNonce, clientPublicKey));

      var sessionToken = body['sessionToken'];
      if (sessionToken == null ||
          !_sessions.containsKey(sessionToken) ||
          _sessions[sessionToken]!.clientPublicKey != clientPublicKey) {
        updateSyncStatus(
            'Client $clientPublicKey attempted to sync with invalid session token');
        return Response.unauthorized('Invalid session token');
      }
      if (!await isPeerAuth(clientPublicKey)) {
        updateSyncStatus(
            'Client $clientPublicKey attempted to sync without authorization');
        return Response.unauthorized('Client key not authorized');
      }
      CrdtChangeset changeSet =
          parseCrdtChangeset(jsonDecode(body['changeset']));

      // Error handling?
      await coreModel.mergeChangeSet(changeSet);
      updateSyncStatus(
          'Successfully merged changeset from client $clientPublicKey');

      syncNonce = keyPair.makeNonce(sodium);
      return Response.ok(
          keyPair.encrypt(
              sodium,
              jsonEncode({
                'status': 'sync_ok',
                'changeset': await coreModel.getChangeSet()
              }),
              syncNonce,
              clientPublicKey),
          headers: {'slatecards-nonce': syncNonce});
    });

    serverHandle =
        await shelf_io.serve(_router, InternetAddress.anyIPv4, servicePort);
  }

  String generateSessionToken({int length = 32}) {
    const chars =
        'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    final random = Random.secure();
    return String.fromCharCodes(
      Iterable.generate(
          length, (_) => chars.codeUnitAt(random.nextInt(chars.length))),
    );
  }

  String generatePIN() {
    Random random = Random();
    return (random.nextInt(9000) + 1000).toString();
  }

  Future<void> stopBroadcast() async {
    await broadcast?.stop();
    broadcast = null;
  }

  bool isBroadcasting() {
    return broadcast != null;
  }
}
