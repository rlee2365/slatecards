import 'package:flutter/material.dart';
import 'package:sqlite_crdt/sqlite_crdt.dart';
import 'package:tsid_dart/tsid_dart.dart';
import 'package:path_provider/path_provider.dart';
import 'package:platform_info/platform_info.dart';
import 'dart:core';
import 'dart:io';
import 'dart:convert';

class Flashcard extends ChangeNotifier {
  String _front = "";
  String _back = "";
  int? position;
  late SqliteCrdt crdt;
  final int id;

  Flashcard({required this.id, required this.crdt});
  Flashcard.fromRecord(
      {required this.id,
      required String front,
      required String back,
      required this.position,
      required this.crdt})
      : _front = front,
        _back = back;

  String get front => _front;
  Future<void> setFront(String front) async {
    _front = front;
    await crdt.execute('''
      UPDATE flashcards SET front = ?1 WHERE id = ?2;
    ''', [front, id]);
    notifyListeners();
  }

  String get back => _back;
  Future<void> setBack(String back) async {
    _back = back;
    await crdt.execute('''
      UPDATE flashcards SET back = ?1 WHERE id = ?2;
    ''', [back, id]);
    notifyListeners();
  }
}

class Deck extends ChangeNotifier {
  String _name = "";
  late DateTime _timeOfCreation;
  late SqliteCrdt crdt;
  final int id;
  int? position;

  String get name => _name;
  Future<void> rename(String name) async {
    _name = name;
    await crdt.execute('''
      UPDATE decks SET name = ?1 WHERE id = ?2;
    ''', [name, id]);
    notifyListeners();
  }

  List<Flashcard> flashcards = [];

  int get length => flashcards.length;

  Deck({required this.id, required String name, required this.crdt})
      : _name = name {
    _timeOfCreation = DateTime.now();
  }

  Deck.fromRecord(
      {required this.id,
      required String name,
      required this.position,
      required this.crdt,
      required DateTime timeOfCreation})
      : _name = name,
        _timeOfCreation = timeOfCreation;

  Future<void> initializeCards() async {
    flashcards.clear();
    final result = await crdt.query('''
      SELECT * from flashcards WHERE is_deleted = 0 AND deck_id = ?1 ORDER BY position ASC;
    ''', [id]);
    for (var m in result) {
      Flashcard f = Flashcard.fromRecord(
        id: m['id'] as int,
        front: m['front'] as String,
        back: m['back'] as String,
        position: m['position'] as int?,
        crdt: crdt,
      );
      flashcards.add(f);
    }
    notifyListeners();
  }

  Future<Flashcard> addCard() async {
    var ret = Flashcard(id: Tsid.getTsid().toLong(), crdt: crdt);
    await crdt.execute('''
      INSERT INTO flashcards (id, position, deck_id, front, back) 
      VALUES (?1, ?2, ?3, ?4, ?5)
      ON CONFLICT DO UPDATE SET is_deleted = 0,
      position = ?2, deck_id = ?3, front = ?4, back = ?5;
    ''', [ret.id, flashcards.length, id, ret.front, ret.back]);
    flashcards.add(ret);
    notifyListeners();
    return ret;
  }

  Future<void> removeCardAt(int index) async {
    await crdt.execute('''
      UPDATE flashcards SET is_deleted = 1, position = NULL WHERE id = ?1;
    ''', [flashcards[index].id]);
    flashcards.removeAt(index);
    await updatePositions();
    notifyListeners();
  }

  Future<void> updatePositions() async {
    for (var (i, f) in flashcards.indexed) {
      f.position = i;
      await crdt.execute('''
        UPDATE decks SET position = ?1 WHERE id = ?2;''', [i, f.id]);
    }
  }

  Flashcard getCard(int index) {
    return flashcards[index];
  }

  Future<void> reorder(int oldIndex, int newIndex) async {
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    final Flashcard card = flashcards.removeAt(oldIndex);
    flashcards.insert(newIndex, card);
    await updatePositions();
    notifyListeners();
  }

  Future<void> reorderInstant(int oldIndex, int newIndex,
      {bool doIndexAdjust = true}) async {
    // doIndexAdjust is because reordering behavior is different for grid and list implementation
    if (doIndexAdjust && oldIndex < newIndex) {
      newIndex -= 1;
    }
    var future = crdt.execute('''
      UPDATE flashcards SET is_deleted = 1, position = NULL WHERE id = ?1;
    ''', [flashcards[oldIndex].id]);
    final Flashcard card = flashcards.removeAt(oldIndex);
    future = future.then((v) {
      crdt.execute('''
        INSERT INTO flashcards (id, deck_id, front, back) 
        VALUES (?1, ?2, ?3, ?4)
        ON CONFLICT DO UPDATE SET is_deleted = 0, deck_id = ?2, front = ?3, back = ?4;
      ''', [card.id, id, card.front, card.back]);
    });
    flashcards.insert(newIndex, card);
    future = future.then((v) {
      updatePositions();
    });
    notifyListeners();
    await future;
  }
}

class Subject extends ChangeNotifier {
  List<Deck> decks = [];
  String _name = "";

  final int id;
  int? position;
  late DateTime _timeOfCreation;
  late SqliteCrdt crdt;

  Subject({required this.id, required String name, required this.crdt})
      : _name = name {
    _timeOfCreation = DateTime.now();
  }

  Subject.fromRecord(
      {required this.id,
      required String name,
      required this.position,
      required this.crdt,
      required DateTime timeOfCreation})
      : _name = name,
        _timeOfCreation = timeOfCreation;

  String get name => _name;
  Future<void> rename(String name) async {
    _name = name;
    await crdt.execute('''
      UPDATE subjects SET name = ?1 WHERE id = ?2;
    ''', [name, id]);
    notifyListeners();
  }

  Future<void> initializeDecks() async {
    decks.clear();
    final result = await crdt.query('''
      SELECT * from decks WHERE is_deleted = 0 AND subject_id = ?1 ORDER BY position ASC;
    ''', [id]);
    for (var m in result) {
      Deck d = Deck.fromRecord(
        id: m['id'] as int,
        name: m['name'] as String,
        position: m['position'] as int?,
        crdt: crdt,
        timeOfCreation: DateTime.parse(m['time_of_creation'] as String),
      );
      await d.initializeCards();
      decks.add(d);
    }
  }

  Future<void> addDeck(String name) async {
    Deck d = Deck(id: Tsid.getTsid().toLong(), name: name, crdt: crdt);
    await crdt.execute('''
      INSERT INTO decks (id, position, subject_id, name, time_of_creation)
      VALUES (?1, ?2, ?3, ?4, ?5)
      ON CONFLICT DO UPDATE SET is_deleted = 0,
      position=?2, subject_id = ?3, name = ?4, time_of_creation = ?5;
    ''', [d.id, decks.length, id, name, _timeOfCreation.toIso8601String()]);
    decks.add(d);
    notifyListeners();
  }

  Future<void> insertDeck(int index, Deck deck) async {
    await crdt.execute('''
      INSERT INTO decks (id, subject_id, name, time_of_creation) 
      VALUES (?1, ?2, ?3, ?4)
      ON CONFLICT DO UPDATE SET is_deleted = 0, subject_id = ?2, name =
      ?3, time_of_creation = ?4;
    ''', [deck.id, id, name, _timeOfCreation.toIso8601String()]);
    decks.insert(index, deck);
    await updatePositions();
    notifyListeners();
  }

  Future<void> updatePositions() async {
    for (var (i, d) in decks.indexed) {
      d.position = i;
      await crdt.execute('''
        UPDATE decks SET position = ?1 WHERE id = ?2;''', [i, d.id]);
    }
  }

  Future<Deck> removeDeck(final Deck deck) async {
    int index = decks.indexOf(deck);
    return removeDeckAt(index);
  }

  Future<Deck> removeDeckAt(final int index, {bool doNotify = true}) async {
    await crdt.execute('''
      UPDATE decks SET is_deleted = 1, position = NULL WHERE id = ?1;
    ''', [decks[index].id]);
    final Deck deck = decks.removeAt(index);
    await updatePositions();
    if (doNotify) {
      notifyListeners();
    }
    return deck;
  }

  Future<void> reorderInstant(int oldIndex, int newIndex) async {
    var future = crdt.execute('''
      UPDATE decks SET is_deleted = 1, position = NULL WHERE id = ?1;
    ''', [decks[oldIndex].id]);
    final Deck deck = decks.removeAt(oldIndex);
    future = future.then((v) {
      crdt.execute('''
      INSERT INTO decks (id, subject_id, name, time_of_creation) 
      VALUES (?1, ?2, ?3, ?4)
      ON CONFLICT DO UPDATE SET is_deleted = 0, subject_id = ?2, name =
      ?3, time_of_creation = ?4;
    ''', [deck.id, id, name, _timeOfCreation.toIso8601String()]);
    });
    decks.insert(newIndex, deck);
    future = future.then((v) {
      updatePositions();
    });
    notifyListeners();
    await future;
  }

  Future<void> reorder(int oldIndex, int newIndex) async {
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    final Deck deck = decks.removeAt(oldIndex);
    decks.insert(newIndex, deck);
    await updatePositions();
    notifyListeners();
  }

  Deck getDeck(int index) {
    return decks[index];
  }

  int deckCount() {
    return decks.length;
  }
}

class CoreModel extends ChangeNotifier {
  List<Subject> subjects = [];
  late SqliteCrdt crdt;

  Future<void> initializeDatabase() async {
    late String dbParent;
    if (platform.isAndroid || platform.isIOS || platform.isWindows) {
      dbParent = (await getApplicationDocumentsDirectory()).path;
    } else {
      dbParent = (await getLibraryDirectory()).path;
    }
    var dbFile = File('$dbParent/slatecards.db');
    crdt = await SqliteCrdt.open('$dbParent/slatecards.db', version: 1,
        //crdt = await SqliteCrdt.openInMemory(
        //version: 1,
        onCreate: (db, version) async {
      await db.execute('''
        CREATE TABLE IF NOT EXISTS subjects (
          id INT PRIMARY KEY,
          position INT,
          name TEXT NOT NULL,
          time_of_creation TEXT NOT NULL
        );''');
      await db.execute('''
        CREATE TABLE IF NOT EXISTS decks (
          id INT PRIMARY KEY,
          position INT,
          subject_id TEXT NOT NULL,
          name TEXT NOT NULL,
          time_of_creation TEXT NOT NULL,
          FOREIGN KEY (subject_id) REFERENCES subjects (id)
        );''');
      await db.execute('''
        CREATE TABLE IF NOT EXISTS flashcards (
          id INT PRIMARY KEY,
          position INT,
          deck_id TEXT NOT NULL,
          front TEXT NOT NULL,
          back TEXT NOT NULL,
          FOREIGN KEY (deck_id) REFERENCES decks (id)
        );
        ''');
    });
  }

  Future<void> initializeData() async {
    subjects.clear();
    final result = await crdt.query('''
      SELECT * from subjects WHERE is_deleted = 0 ORDER BY position ASC;
      ''');
    for (var m in result) {
      Subject s = Subject.fromRecord(
          id: m['id'] as int,
          name: m['name'] as String,
          position: m['position'] as int?,
          crdt: crdt,
          timeOfCreation: DateTime.parse(m['time_of_creation'] as String));
      await s.initializeDecks();
      subjects.add(s);
    }
  }

  Future<void> addSubject(String name) async {
    Subject s = Subject(id: Tsid.getTsid().toLong(), name: name, crdt: crdt);
    await crdt.execute('''
      INSERT INTO subjects (id, position, name, time_of_creation) VALUES (?1, ?2, ?3, ?4)
      ON CONFLICT DO UPDATE SET is_deleted = 0, position=?2, name = ?3, time_of_creation = ?4;
    ''', [
      s.id,
      subjects.length, // position of new subject
      name,
      s._timeOfCreation.toIso8601String()
    ]); // we should update is_deleted
    subjects.add(s);

    notifyListeners();
  }

  Future<void> insertSubject(int index, Subject subject) async {
    await crdt.execute('''
      INSERT INTO subjects (id, name, time_of_creation) VALUES (?1, ?2, ?3)
      ON CONFLICT DO UPDATE SET is_deleted = 0, name = ?2, time_of_creation = ?3;
    ''', [subject.id, subject.name, subject._timeOfCreation.toIso8601String()]);
    subjects.insert(index, subject);
    // Because an insertion potentially updates all of the subject positions, we do:
    await updatePositions();
    notifyListeners();
  }

  Future<void> updatePositions() async {
    for (var (i, s) in subjects.indexed) {
      s.position = i;
      await crdt.execute('''
        UPDATE subjects SET position = ?1 WHERE id = ?2;''', [i, s.id]);
    }
  }

  //// Performs reodering in model while waiting for DB query to execute
  Future<void> reorderInstant(int oldIndex, int newIndex) async {
    var future = crdt.execute('''
      UPDATE subjects SET is_deleted = 1, position = NULL WHERE id = ?1;
    ''', [subjects[oldIndex].id]);
    final Subject subject = subjects.removeAt(oldIndex);
    future = future.then((v) {
      crdt.execute('''
      INSERT INTO subjects (id, name, time_of_creation) VALUES (?1, ?2, ?3)
      ON CONFLICT DO UPDATE SET is_deleted = 0, name = ?2, time_of_creation = ?3;
    ''', [subject.id, subject.name, subject._timeOfCreation.toIso8601String()]);
    });
    subjects.insert(newIndex, subject);
    future = future.then((v) {
      updatePositions();
    });
    notifyListeners();
    await future;
  }

  Future<Subject> removeSubjectAt(final int index,
      {bool doNotify = true}) async {
    await crdt.execute('''
      UPDATE subjects SET is_deleted = 1, position = NULL WHERE id = ?1;
    ''', [subjects[index].id]);
    final Subject subject = subjects.removeAt(index);
    await updatePositions();
    if (doNotify) {
      notifyListeners();
    }
    return subject;
  }

  Future<Subject> removeSubject(final Subject subject) async {
    int index = subjects.indexOf(subject);
    return removeSubjectAt(index);
  }

  Future<void> reorder(int oldIndex, int newIndex) async {
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    final Subject subject = subjects.removeAt(oldIndex);
    subjects.insert(newIndex, subject);
    await updatePositions();
    notifyListeners();
  }

  Subject getSubject(int index) {
    return subjects[index];
  }

  int subjectCount() {
    return subjects.length;
  }

  Future<String> getChangeSet({Hlc? after}) async {
    return jsonEncode(await crdt.getChangeset(modifiedAfter: after));
  }

  Future<Hlc?> getLatestHlc() async {
    // no ordering is guaranteed here
    var changeset = await crdt.getChangeset();
    if (changeset.isEmpty) {
      return null;
    }
    Hlc maxHlc = Hlc.zero(nodeId());
    for (var tableEntry in changeset.entries) {
      for (var row in tableEntry.value) {
        if (row.containsKey('hlc') && ['hlc']! as Hlc > maxHlc) {
          maxHlc = row['hlc']! as Hlc;
        }
      }
    }
    return maxHlc;
  }

  String nodeId() {
    return crdt.nodeId;
  }

  Future<void> mergeChangeSet(CrdtChangeset changeset) async {
    await crdt.merge(changeset);
    await initializeData();
    notifyListeners();
  }
}

class CoreModelSingle {
  static final CoreModel coreModel = CoreModel();

  static Future<void> init() async {
    await coreModel.initializeDatabase();
    await coreModel.initializeData();
  }
}
